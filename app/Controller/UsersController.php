<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {
		
		public function beforeFilter() {
				parent::beforeFilter();
				// Allow users to register and logout.
				$this->Auth->allow('add', 'logout');
		}

		public function login() {
				if ($this->request->is('post')) {
						if ($this->Auth->login()) {
								return $this->redirect($this->Auth->redirect());
						}						
						$this->setFlash('Invalid username or password, try again', 'error');
				}
		}

		public function logout() {
				return $this->redirect($this->Auth->logout());
		}


    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->setFlash('The user has been saved', 'success');
                return $this->redirect(array('action' => 'index'));
            }
            $this->setFlash('The user could not be saved. Please, try again.', 'error');
        }
    }

    public function edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->setFlash('The user has been saved');
                return $this->redirect(array('action' => 'index'));
            }
            $this->setFlash('The user could not be saved. Please, try again.', 'error');
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        $this->request->onlyAllow('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->setFlash('User deleted', 'success');
            return $this->redirect(array('action' => 'index'));
        }
        $this->setFlash('User was not deleted', 'error');
        return $this->redirect(array('action' => 'index'));
    }

}
