<?php $params = $this->Session->read('Message.flash.params'); ?>
<div class="alert alert-<?php echo !empty($params['class']) ? $params['class']: 'warning' ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>	
  <?php echo h($message); ?>
</div>