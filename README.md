CakePHP Basic App
=================
Working copy of a basic CakePHP app. It includes:
1. CakePHP 2.4.3
2. Debug Tookit
3. Localized
4. Twitter Bootstrap
5. User signup, login, logout, edit etc base on (http://book.cakephp.org/2.0/en/tutorials-and-examples/blog-auth-example/auth.html)


INSTALLATION:
==================
Clone the repository to local computer and run the following command from the directory.

1. chmod 777 -R app/tmp
2. git submodule update --init
3. cp app/Config/database.php.default app/Config/database.php and update the database credentials
4. Setup vhost configuration